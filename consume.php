<?php
require 'vendor/autoload.php';

use Monolog\Handler\ErrorLogHandler;
use Monolog\Logger;

// Create the logger
$logger = new Logger('my_logger');
// Now add some handlers
$logger->pushHandler(new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, Logger::INFO));

$config = \Kafka\ConsumerConfig::getInstance();
$config->setMetadataRefreshIntervalMs(10000);
$config->setMetadataBrokerList('172.19.0.2:9092');
$config->setGroupId('fnord');
//$config->setBrokerVersion('0.9.0.1');
$config->setTopics(array('testchen'));
//$config->setOffsetReset('earliest');
$consumer = new \Kafka\Consumer();
$consumer->setLogger($logger);
$consumer->start(function ($topic, $part, $message) {
    var_dump($message);
});