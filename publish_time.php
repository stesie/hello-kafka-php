<?php
require 'vendor/autoload.php';

use Monolog\Handler\ErrorLogHandler;
use Monolog\Logger;

// Create the logger
$logger = new Logger('my_logger');
// Now add some handlers
$logger->pushHandler(new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, Logger::INFO));

$config = \Kafka\ProducerConfig::getInstance();
$config->setMetadataRefreshIntervalMs(10000);
$config->setMetadataBrokerList('172.19.0.2:9092');
//$config->setBrokerVersion('0.9.0.1');
//$config->setRequiredAck(1);
$config->setIsAsyn(false);
//$config->setProduceInterval(500);
$producer = new \Kafka\Producer();
$producer->setLogger($logger);

for (;;) {
    $result = $producer->send(array(
        array(
            'topic' => 'testchen',
            'value' => 'aktuelle Zeit: ' . date('Ymd-His'),
            'key' => 'blub',
            //'partId' => 2,
        ),
    ));
    var_dump($result);
    sleep(1);
}